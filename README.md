**Tag 1.0: initial state from cli**


Example of commands used:

`$ ionic start demo-to-do-app`

`$ git checkout tag/1.0`

**Tag 1.1: todo model + cards + directives**

**Tag 1.2: new add to do page + [] vs () vs [()] + navigation, modules imports**

Example of commands used:

`$ ionic g page add`

**Tag 1.3: added new to do form, to do list set and retrieved from storage**

Example of commands used:

`$ ionic g provider ToDoProvider`

`$ ionic g component TodoCard`

Note following tags and commits only exist in the dev branch

**Tag 1.4: custom service, custom component**

**Tag 1.5: added geolocation plugin + modal component + calculating distance**

Example of commands used:

`$ ionic cordova plugin add cordova-plugin-geolocation --variable GEOLOCATION_USAGE_DESCRIPTION="To locate you"`

`$ npm install --save @ionic-native/geolocation`

**Tag 1.6:  custom theme via scss**

Slides: https://drive.google.com/open?id=1GJrifUf9BecQUu7ryBDXM_z28YXSCNjJP-NNL-bZyeo

Compiled application for Android: https://drive.google.com/open?id=1DkZiljGws-DmLkqYfi_CZiEK5s77j8Mv
