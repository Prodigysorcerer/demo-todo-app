import { BrowserModule } from '@angular/platform-browser';
import { ErrorHandler, NgModule } from '@angular/core';
import { IonicApp, IonicErrorHandler, IonicModule } from 'ionic-angular';
import { IonicStorageModule } from "@ionic/storage";
import { SplashScreen } from '@ionic-native/splash-screen';
import { StatusBar } from '@ionic-native/status-bar';
import { Geolocation } from "@ionic-native/geolocation";

import { MyApp } from './app.component';
import { HomePage } from '../pages/home/home';
import { AddPage } from '../pages/add/add';
import { AddPageModule } from '../pages/add/add.module';
import { TodoProvider } from '../providers/todo/todo';
import { ToDoCardComponent } from '../components/to-do-card/to-do-card';
import { ToDoMapComponent } from "./../components/to-do-map/to-do-map";


@NgModule({
  declarations: [MyApp, HomePage, ToDoCardComponent, ToDoMapComponent],
  imports: [
    BrowserModule,
    IonicModule.forRoot(MyApp),
    IonicStorageModule.forRoot(),
    AddPageModule
  ],
  bootstrap: [IonicApp],
  entryComponents: [MyApp, HomePage, AddPage, ToDoMapComponent],
  providers: [
    StatusBar,
    SplashScreen,
    { provide: ErrorHandler, useClass: IonicErrorHandler },
    TodoProvider,
    Geolocation
  ]
})
export class AppModule {}
