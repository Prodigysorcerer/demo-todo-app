import { Component } from '@angular/core';
import * as leaflet from 'leaflet';

/**
 * Generated class for the ToDoMapComponent component.
 *
 * See https://angular.io/api/core/Component for more info on Angular
 * Components.
 */
@Component({
  selector: 'to-do-map',
  templateUrl: 'to-do-map.html'
})
export class ToDoMapComponent {

  constructor() {

  }

  ionViewDidEnter(){
    let map = new leaflet.Map('mapid');

    // create the tile layer with correct attribution
    let osmUrl = 'https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png';
    let osmAttrib = 'Map data © <a href="https://openstreetmap.org">OpenStreetMap</a> contributors';
    let osm = new leaflet.TileLayer(osmUrl, { minZoom: 8, maxZoom: 12, attribution: osmAttrib });

    // start the map in South-East England
    map.setView(new leaflet.LatLng(51.3, 0.7), 9);
    map.addLayer(osm);

    map.on('click', () => {
      console.log("click");
    });
  }

}
