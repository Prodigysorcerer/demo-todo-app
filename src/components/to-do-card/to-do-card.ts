import { Component, Input, Output, EventEmitter } from '@angular/core';
import { ToDoModel } from '../../models/todo.model';
import { AlertController } from "ionic-angular";

@Component({
  selector: "to-do-card",
  templateUrl: "to-do-card.html"
})
export class ToDoCardComponent {
  @Input() todo: ToDoModel = null;
  @Input() showDone: boolean = false;
  @Input() todoIndex: number = null;
  @Output() deleteIndex = new EventEmitter<number>();
  @Output() doneIndex =  new EventEmitter<number>();
  @Input() lat: number = null;
  @Input() long: number = null;

  constructor(private alertCtrl: AlertController) {}

  delete() {
    let alert = this.alertCtrl.create({
      title: "Confirm delete",
      message: "Are you sure you want to delete this item ?",
      buttons: [
        {
          text: "Cancel",
          role: "cancel",
          handler: () => {
            console.log("Cancel clicked");
          }
        },
        {
          text: "Delete",
          handler: () => {
            this.deleteIndex.emit(this.todoIndex);
          }
        }
      ]
    });
    alert.present();
  }

  toggleDone() {
    this.doneIndex.emit(this.todoIndex);
  }

  /* I would like to thank StackOverflow for this code */
  // TODO: extract this into a helper function
  calculateDistance(): number {
    let R = 6371; // Radius of the earth in km
    let dLat = this.deg2rad(this.lat - this.todo.latittude);  // deg2rad below
    let dLon = this.deg2rad(this.long - this.todo.longitude);
    let a =
      Math.sin(dLat / 2) * Math.sin(dLat / 2) +
      Math.cos(this.deg2rad(this.lat)) * Math.cos(this.deg2rad(this.todo.latittude)) *
      Math.sin(dLon / 2) * Math.sin(dLon / 2);
    let c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1 - a));
    let d = R * c; // Distance in km
    return Math.floor(d);
  }

  deg2rad(deg: number) {
    return deg * (Math.PI / 180)
  }
}
