import { Component } from '@angular/core';
import { NavController } from 'ionic-angular';
import { Geolocation } from "@ionic-native/geolocation";
import { ToDoModel } from '../../models/todo.model';
import { AddPage } from '../add/add';
import { TodoProvider } from '../../providers/todo/todo';

@Component({
  selector: "page-home",
  templateUrl: "home.html"
})
export class HomePage {
  public todoList: ToDoModel[] = [];
  public showDone: boolean = false;
  public lat: number = null;
  public long: number = null;

  constructor(
    public navCtrl: NavController,
    private geolocation: Geolocation,
    private toDoProvider: TodoProvider
  ) {
    this.initList();
    this.initPosition()
  }

  async initList() {
    this.todoList = await this.toDoProvider.getToDoList();
  }

  async initPosition() {
    try {
      let response: any = await this.geolocation.getCurrentPosition();
      this.lat = response.coords.latitude;
      this.long = response.coords.longitude;
    } catch {
      console.warn("No soup for you!!!");
    }
  }

  addToDo() {
    this.navCtrl.push(AddPage);
  }

  deleteToDo(index: number) {
    this.todoList.splice(index, 1);
    this.toDoProvider.saveToDoList(this.todoList);
  }

  toggleDone(index: number) {
    this.todoList[index].done = !this.todoList[index].done;
    this.toDoProvider.saveToDoList(this.todoList);
  }
}
