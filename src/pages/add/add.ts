import { Component } from '@angular/core';
import {
  IonicPage,
  NavController,
  NavParams,
  ModalController
} from "ionic-angular";
import { ToDoModel } from '../../models/todo.model';
import { HomePage } from '../home/home';
import { TodoProvider } from '../../providers/todo/todo';
import { ToDoMapComponent } from '../../components/to-do-map/to-do-map';

@IonicPage()
@Component({
  selector: "page-add",
  templateUrl: "add.html"
})
export class AddPage {
  public continueAdding: boolean = true;
  public newToDo: ToDoModel = new ToDoModel();

  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    private toDoProvider: TodoProvider,
    private modalCtrl: ModalController
  ) {}

  addLocation() {
    let mapModal = this.modalCtrl.create(ToDoMapComponent);
    mapModal.present();
  }

  async add() {
    await this.toDoProvider.addToDo(this.newToDo);
    if (this.continueAdding) {
      this.newToDo = new ToDoModel();
      this.newToDo.text = null;
    } else {
      this.navCtrl.push(HomePage);
    }
  }

  switchContinue() {
    this.continueAdding = !this.continueAdding;
  }
}
