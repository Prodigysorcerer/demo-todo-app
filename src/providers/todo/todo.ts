import { Storage } from '@ionic/storage';
import { Injectable } from '@angular/core';
import { ToDoModel } from '../../models/todo.model';

@Injectable()
export class TodoProvider {

  constructor(private storage: Storage) {
  }

  public async getToDoList(): Promise<ToDoModel[]> {
    let list = [];
    const raw = await this.storage.get("list");
    if (raw) {
      list = JSON.parse(raw);
    }
    return Promise.resolve(list);
  }

  public async addToDo(item: ToDoModel): Promise<boolean> {
    let list = [];
    const raw = await this.storage.get("list");
    if (raw) {
      list = JSON.parse(raw);
    }
    list.push(item);
    this.storage.set("list", JSON.stringify(list));
    return Promise.resolve(true);
  }

  public saveToDoList(list: ToDoModel[]) {
    this.storage.set("list", JSON.stringify(list));
  }

}
