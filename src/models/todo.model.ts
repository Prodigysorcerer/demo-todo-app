export class ToDoModel {
    public text: string;
    public done: boolean = false;
    public latittude: number = null;
    public longitude: number = null;
}